import cp = require('child_process');
import path = require('path');
import yargs = require("yargs");
import fs = require('fs');

class Startup {
    public static main(): number {
        const prj_dir = path.basename(process.cwd()).replace(/\./g, '-');

        const homedir = require('os').homedir();
        const cfgpath = path.join(homedir, ".gitlab.json");
        if (!fs.existsSync(cfgpath)) {
            fs.writeFileSync(cfgpath, "{}");
        }

        const argv = yargs.options({
            user: {
                alias: 'u',
                type: 'string',
                description: 'gitlab user name'
            },
            printonly: {
                alias: 'p',
                type: 'boolean',
                description: "print-only, doesn't issue git commands",
                default: false
            }
        }).argv;

        console.log('printonly: ' + argv.printonly);
        var json: { user: string; };
        json = require(cfgpath);

        if (argv.user) {
            json.user = argv.user;
            fs.writeFile(cfgpath, JSON.stringify(json, null, 4), (err) => {
                if (err) {
                    console.error(err);
                    process.exit(1);
                };
            });
        } else {
            if (!json?.user) {
                console.log("Error: missing username, use --user option");
                process.exit(1);
            }
        }

        let reqd = [
            ".gitignore",
            "README.md"
        ];
        reqd.forEach(function (value: string) {
            Startup.file_assert(value);
        });


        let cmds = [
            // "cd prj_dir",
            "git init",
            "git add .",
            'git commit -m "intial commit"',
            `git push --set-upstream https://gitlab.com/${json.user}/${prj_dir}.git master`,
            `git remote add origin https://gitlab.com/${json.user}/${prj_dir}.git`,
            "git pull"
        ];
        cmds.forEach(function (value) {
            console.log(value);
            try {
                if (!argv.printonly) {
                    cp.execSync(value);
                }
            }
            catch (e) {
                console.log('Error', e);
                process.exit(1);
            }

        });

        return 0;
    }

    static file_assert(fn: string): void {
        if (!fs.existsSync(fn)) {
            console.log("Error: must have " + fn);
            process.exit(1);
        }
    }
}

Startup.main();