# Gitlab helper npm utility

This is built in vscode. Check the backfround [info](https://kkibria.github.io/dev_cookbook/topics/project.html).

## Prerequisites
You must have ``nodejs`` installed to have ``npm``. For ubuntu I used,
```
sudo apt install nodejs-legacy
```

You will have to get all the required node packages. from the project directory run,
```
npm install
```

## Build
For build only
```bash
npm run build
```
For building + live watching
```bash
npm start
```
This will put the typescript compiling in watch mode which is helpful during development debugging.

## Test
```
npm run test
```
will run the utility in print only mode. If this is the first time, it will complain about user name. Following will set the gitlab user name,

```bash
node dist -p -user <gitlab_user_name>
```
This will save your gitlab user name in your home directory in ``.gitlab.json`` setting file. You only need to this once, any future use will use the name from this file.

## Install
```bash
npm pack
#pack produces the install archieve  gitlab-<?.?.?>.tgz
npm i -g gitlab-<?.?.?>.tgz 
```
will install the utilty globally.

## Running the utility
```bash
cd <project_directory>
gitlab
```
will create a project in gitlab and set up git to use gitlab project as ``remote master`` for the project.
